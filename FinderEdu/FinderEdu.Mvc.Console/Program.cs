﻿using System;
using static System.Console;

namespace FinderEdu.Mvc.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Write("Enter A : ");
            var aStr = ReadLine();
            Write("Enter B : ");
            var bStr = ReadLine();

            var a = int.Parse(aStr);
            var b = int.Parse(bStr);
            var sum = a + b;

            WriteLine($"Sum = {sum}");
            ReadKey();
        }
    }
}
