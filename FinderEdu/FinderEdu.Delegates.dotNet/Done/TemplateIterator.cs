﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinderEdu.Delegates.dotNet.Done
{

    public static class TemplateIterator
    {
        public static void ForAll<T>(
            IEnumerable<T> items, 
            Action<T> action)
        {
            foreach(T item in items)
            {
                action(item);
            }
        }

        // When <T> is int
        public static void ForAllInt(int[] array, Action<int> action)
        {
            for (int i = 0; i < array.Length; i++)
            {
                action(array[i]);
            }
        }

        // When <T> is int
        public static void ForAllInt2(int[] array, Action<int> action)
        {
            TemplateIterator.ForAll<int>(array, action);
        }

        // When <T> is double
        public static void ForAllDouble(double[] array, Action<double> action)
        {
            for (int i = 0; i < array.Length; i++)
            {
                action(array[i]);
            }
        }

        // When <T> is double. Simplified
        public static void ForAllDouble2(double[] array, Action<double> action)
        {
            ForAll(array, action);
        }
    }
}
