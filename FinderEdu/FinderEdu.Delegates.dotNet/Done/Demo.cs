﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinderEdu.Delegates.dotNet.Done
{
    public class Demo
    {
        private int[] integers = { 5, -12, 23, 65, -15 };

        private List<double> doubles =
            new List<double> { 5.1, 12.2, 23.3, 65.4, 15.5 };

        public void Show()
        {
            //SimpleIterator1();
            //SimpleIterator2();
            //SimpleIterator3();
            //SimpleIterator4();

            TemplateIterator1();
            TemplateIterator2();
        }

        #region Simple iterator
        private void PrintInt(int item)
        {
            Console.WriteLine(item);
        }

        private void SimpleIterator1()
        {
            Console.WriteLine("[S1. Simple iterator with delegate]:");

            Iterator.ForAll(integers, PrintInt);
        }

        private void SimpleIterator2()
        {
            Console.WriteLine("[S2. Iterartor with 'anonymous method']:");

            Processor printInt = delegate (int item) 
            {
                Console.WriteLine("{0}^2 = {1}", item, item*item);
            };

            Iterator.ForAll(integers, printInt);
        }

        private void SimpleIterator3()
        {
            Console.WriteLine("[S3. Iterartor with lambda and 'anonymous method']:");

            // 1. standard
            Processor printDel = PrintInt;
            // 2. anonymous
            Processor printAnon = delegate (int item)
            {
                Console.WriteLine("{0}^2 = {1}", item, item * item);
            };
            // 3. lambda
            Processor printLabmda = (int item) => Console.WriteLine("{0}^3 = {1}", item, Math.Pow(item, 3));

            Iterator.ForAll(integers, printDel);
            Iterator.ForAll(integers, printAnon);
            Iterator.ForAll(integers, printLabmda);
        }

        private void SimpleIterator4()
        {
            Console.WriteLine("[S4. Iterartor with lambda (simplified)]:");

            Iterator.ForAll(integers, item => Console.WriteLine("{0}^3 = {1}", item, Math.Pow(item, 3))
            );
        }
        #endregion

        #region Templated iterator
        private void TemplateIterator1()
        {
            Console.WriteLine("[T1. Templated iterartor with lambda]:");

            Console.WriteLine("Sqr of integers");
            TemplateIterator.ForAll(
                integers, 
                item => Console.WriteLine("{0}^2 = {1}", item, item * item)
            );
        }

        private void TemplateIterator2()
        {
            Console.WriteLine("[T2. Templated iterartor with lambda and actions]:");

            Action<double> del = 
                item =>  Console.WriteLine("{0}^1/2 = {1}", item, Math.Sqrt(item));

            TemplateIterator.ForAll<double>(doubles, del);
        }
        #endregion
    }
}
