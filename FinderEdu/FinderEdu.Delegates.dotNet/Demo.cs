﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinderEdu.Delegates.dotNet
{
    public class Demo
    {
        private int[] integers = { 5, -12, 23, 65, -15 };

        public void Show()
        {
            Sample1();
        }

        private void PrintInt(int item)
        {
            Console.WriteLine(item);
        }

        private void Sample1()
        {
            Iterator.ForAll(integers, PrintInt);
        }
    }
}
