﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FinderEdu.Linq
{
    public class ToDoItem
    {
        public Guid Id { get; set; }
        // Change OwnerID : Guid to Owner : string
        public string Owner { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }

        public ToDoItem()
        {
            Id = Guid.NewGuid();
            CreatedAt = DateTime.Now;
        }

        public ToDoItem(Guid id, DateTime createdAt)
        {
            Id = id;
            CreatedAt = createdAt;
        }

        public int GetDescriptionLength()
        {
            if (string.IsNullOrEmpty(Description)) return 0;
            return Description.Length;
        }
    }

    /// <summary>
    /// Compare ToDo items by ID property
    /// </summary>
    public class ToDoItemIdComparer : IEqualityComparer<ToDoItem>
    {
        public bool Equals(ToDoItem x, ToDoItem y)
        {
            if (Object.ReferenceEquals(x, y)) return true;

            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            return x.Id.Equals(y.Id);
        }

        public int GetHashCode(ToDoItem model)
        {
            if (Object.ReferenceEquals(model, null)) return 0;
            return model.Id.GetHashCode();
        }
    }

    public class Demo
    {
        private  ToDoItem[] Items;
        private  List<ToDoItem> Updates;

        public void Show()
        {
            FillCollections();
            //LinqSelect();
            //LinqWhere();
            //LinqAvg();
            //LinqOrder();
            LinqIntersects();
        }

        private void FillCollections()
        {
            var myItem1 = new ToDoItem()
            {
                Title = "Buy fruits",
                Description = "Be ready for party!",
                Owner  = "Dima",
                CreatedAt = DateTime.Now.AddHours(-1) // hour ago
            };

            var presentationID = Guid.NewGuid();
            var bdayID = Guid.NewGuid();

            var myItem2 = new ToDoItem()
            {
                Id = presentationID,
                Title = "Presentation",
                Description = "Visit KPI at 14:00",
                Owner = "Dima",
                CreatedAt = DateTime.Now.AddDays(-2) // 2 days ago
            };

            var myItem3 = new ToDoItem()
            {
                Id = bdayID,
                Title = "Ivan",
                Description = "Ivan birthday (friday)",
                Owner = "Dima",
                CreatedAt = DateTime.Now // just created
            };

            var myItem4 = new ToDoItem()
            {
                Id = bdayID,
                Title = "Taxes",
                Description = "Pay taxes",
                Owner = "Dima",
                CreatedAt = DateTime.Now.AddMonths(-1) // month ago
            };

            var updatedItem2 = new ToDoItem()
            {
                Id = presentationID,
                Title = "Presentation (updated)",
                Description = "Visit KPI at 16:00",
                Owner = "Dima",
                CreatedAt = DateTime.Now.AddDays(-2) // 2 days ago
            };

            var updatedItem3 = new ToDoItem()
            {
                Id = bdayID,
                Title = "Ivan (UPD)",
                Description = "Ivan birthday. Moved to Sunday",
                Owner = "Dima",
                CreatedAt = DateTime.Now // just created
            };

            // Array and List<> are different, but both implements IEnumerable
            // Check Enumerable class extensions

            Items = new ToDoItem[] 
            {
                myItem1,
                myItem2,
                myItem3,
                myItem4
            };

            Updates = new List<ToDoItem> ()
            {
                updatedItem2,
                updatedItem3
            };
        }

        private void LinqIntersects()
        {
            Console.WriteLine("[Linq intersect]");
            Console.WriteLine("Select titles of updated items : ");

            var titles = Items
                .Intersect(Updates, new ToDoItemIdComparer())
                .Select(item => item.Title);

            foreach (var title in titles)
            {
                Console.WriteLine(title);
            }

            Console.ReadKey();
        }

        private void LinqAvg()
        {
            Console.WriteLine("[Linq Average]");
            Console.WriteLine("Compute average description length of Items");

            var avgDescriptionLength =
                Items.Average(item => item.GetDescriptionLength());

            Console.WriteLine($"Averege lenth is {avgDescriptionLength}");
            Console.ReadKey();
        }

        private void LinqOrder()
        {
            Console.WriteLine("[Linq Order by]");
            Console.WriteLine("Select titles of ordered items ");

            IEnumerable<string> titles =
                Items
                .OrderByDescending(item => item.CreatedAt)
                .Select(item => item.Title);

            foreach (var title in titles) Console.WriteLine(title);

            Console.ReadKey();
        }

        private void LinqWhere()
        {
            Console.WriteLine("[Linq Where]");
            Console.WriteLine("Select titles of old items (created more than day ago) : ");

            var dayAgo = DateTime.Now.AddDays(-1);

            IEnumerable<string> titles = 
                Items
                .Where(item=>item.CreatedAt < dayAgo)
                .Select(item => item.Title);

            foreach (var title in titles)
            {
                Console.WriteLine(title);
            }

            Console.ReadKey();
        }

        private void LinqSelect()
        {
            Console.WriteLine("[Linq select]");
            Console.WriteLine("Select all titles : ");

            IEnumerable<string> titles = 
                Items.Select(ExtarctTitle);

            foreach (var title in titles)
            {
                Console.WriteLine(title);
            }

            Console.ReadKey();
        }

        private static string ExtarctTitle(ToDoItem item)
        {
            return item.Title;
        }

    }
}
