﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinderEdu.Delegates.Tpl
{
    public class Demo
    {
        public static double AvgRandom(int count)
        {
            var random = new Random();
            double sum = 0.0;
            for(int i = 0; i < count; i++)
            {
                sum += random.NextDouble();
            }
            return sum / count;
        }
    }
}
