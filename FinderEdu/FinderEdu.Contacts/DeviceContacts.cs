﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinderEdu.Contacts
{
    public class DeviceContacts : IContactsSource
    {
        public Contact[] GetContacts()
        {
            return new Contact[] 
            {
                new Contact("Max", "044-571-42-30"),
                new Contact("Merry", "044-333-96-17"),
                new Contact("Sarra Connor", "056-123-45-67"),
            };
        }
    }
}
