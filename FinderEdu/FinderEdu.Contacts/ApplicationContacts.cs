﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinderEdu.Contacts
{
    public class ApplicationContacts : IContactsSource
    {
        public Contact[] GetContacts()
        {
            return new Contact[] 
            {
                new Contact("Mom", "032-117-75-03"),
                new Contact("Sister", "032-783-34-28"),
                new Contact("Bro", "032-247-99-66"),
            };
        }
    }
}
