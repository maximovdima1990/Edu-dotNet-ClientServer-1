﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinderEdu.Contacts
{
    public class Google : IContactsSource
    {
        public Contact[] GetContacts()
        {
            return new Contact[] 
            {
                new Contact("Boss", "044-777-77-77"),
                new Contact("Project mngr", "044-666-66-66"),
            };
        }
    }
}
