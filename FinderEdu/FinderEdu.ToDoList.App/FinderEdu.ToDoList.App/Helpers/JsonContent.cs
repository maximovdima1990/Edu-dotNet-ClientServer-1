﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace FinderEdu.ToDoList.App.Helpers
{
    public class JsonContent : StringContent
    {
        private const string MediaType = "application/json";

        public JsonContent(string json)
            : base(json)
        {
            Headers.ContentType = new MediaTypeHeaderValue(MediaType);
        }

        public static JsonContent FromObject(object obj)
        {
            var json = obj.ToJson();
            return new JsonContent(json);
        }
    }
}
