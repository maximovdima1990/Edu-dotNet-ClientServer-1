﻿using FinderEdu.ToDoList.App.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinderEdu.ToDoList.App
{
    public class RemoteDataStore : IDataStore<Item>
    {
        private readonly string _userId = "00000000-0000-0000-0000-000000000001";
        private readonly IServer _server;

        List<Item> _items = new List<Item>();
        private bool _inSync = false;

        public RemoteDataStore()
        {
            _server = ServiceLocator.Instance.Get<IServer>();
        }

        public async Task<bool> AddItemAsync(Item item)
        {
            await _server.AddOrUpdateItemAsync(_userId, item);
            _items.Add(item);

            return true;
        }

        public async Task<bool> UpdateItemAsync(Item item)
        {
            await _server.AddOrUpdateItemAsync(_userId, item);

            var oldItem = _items.FirstOrDefault(old => old.Id == item.Id);
            if (oldItem != null) _items.Remove(oldItem);
            _items.Add(item);

            return true;
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            await _server.DeleteItemAsync(_userId, id);

            var oldItem = _items.FirstOrDefault(old => old.Id == id);
            if (oldItem != null) _items.Remove(oldItem);

            return true;
        }

        public async Task<Item> GetItemAsync(string id)
        {
            return await Task.FromResult(_items.FirstOrDefault(item => item.Id == id));
        }

        public async Task<IEnumerable<Item>> GetItemsAsync(bool forceRefresh = false)
        {
            if (!_inSync || forceRefresh)
            {
                _items = await _server.LoadAllItemsAsync(_userId);
                _inSync = true;
            }

            return _items;
        }
    }
}
