﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinderEdu.ToDoList.App.Services
{
    public interface IServer
    {
        Task AddOrUpdateItemAsync(string userId, Item item);
        Task DeleteItemAsync(string userId, string itemId);
        Task<List<Item>> LoadAllItemsAsync(string userId);
    }
}
