﻿using System;

namespace FinderEdu.ToDoList.App
{
    public class Item
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public Item()
        {
            Id = Guid.NewGuid().ToString();
        }

        public override string ToString()
        {
            return $"{Title} [{Description}]";
        }
    }
}
