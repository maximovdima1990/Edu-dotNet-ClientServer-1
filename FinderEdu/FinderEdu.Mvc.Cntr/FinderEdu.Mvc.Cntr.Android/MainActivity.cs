﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace FinderEdu.Mvc.Cntr.Droid
{
	[Activity (Label = "FinderEdu.Mvc.Cntr.Android", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
        Counter counter = new Counter(0, 10);
        Button button;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			// Get our button from the layout resource,
			// and attach an event to it
			button = FindViewById<Button> (Resource.Id.myButton);

            button.Click += OnCounterButtonClick;
		}

        private void OnCounterButtonClick(object sender, EventArgs e)
        {
            if (counter.Count()) button.Text = $"Clicks : {counter.Value}";
        }
    }
}


