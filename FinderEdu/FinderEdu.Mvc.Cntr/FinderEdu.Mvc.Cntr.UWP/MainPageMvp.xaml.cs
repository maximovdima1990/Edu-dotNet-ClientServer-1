﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using FinderEdu.Mvc.Cntr;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace FinderEdu.Mvc.Cntr.UWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPageMvp : Page, ICounterPage
    {
        private readonly Presenter _presenter;

        public MainPageMvp()
        {
            this.InitializeComponent();

            _presenter = new Presenter(this);
        }

        public event EventHandler CountRequested;

        public void SetCounts(int c)
        {
            cntrText.Text = $"Clicks : {c}";
            cntrText2.Text = $"Clicks : {c}; Last : {DateTime.Now}";
        }

        private void CntrButtonClick(object sender, RoutedEventArgs e)
        {
            //if (counter.Count()) cntrText.Text = $"Clicks : {counter.Value}";

            CountRequested?.Invoke(this, EventArgs.Empty);
        }
    }
}