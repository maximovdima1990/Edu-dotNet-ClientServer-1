﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinderEdu.Mvc.Cntr.UWP
{
    public interface ICounterPage
    {
        event EventHandler CountRequested;
        void SetCounts(int c);
    }
}
