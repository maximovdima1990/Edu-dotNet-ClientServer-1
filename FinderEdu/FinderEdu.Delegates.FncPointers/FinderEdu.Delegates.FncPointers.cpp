#include "stdafx.h"

using namespace std;

void ForAll(int items[], int length, 
	void(*pfunc)(int))
{
	for (int i = 0; i < length; i++)
	{
		(*pfunc)(items[i]);
	}
}

void PrintItem(int item)
{
	cout << item << endl;
}

void PrintItemSquare(int item)
{
	cout << item << " ^ 2 = " << item*item << endl;
}


int main()
{
	cout << "[Function pointers demo]" << endl;

	const int n = 5;
	int items[n] = { 5, -12, 23, 65, -15 };

	ForAll(items, n, PrintItemSquare);

	cin.get();
	return 0;
}