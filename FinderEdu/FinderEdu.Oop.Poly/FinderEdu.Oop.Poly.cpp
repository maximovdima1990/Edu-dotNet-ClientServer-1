// FinderEdu.Oop.Poly.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

using namespace std;

class Simple
{
private:
	int m_id;

public:
	Simple(int id)
	{
		setID(id);
	}

	int* getIdFieldAddress()
	{
		return &m_id;
	}


	int getID() { return m_id; }
	
	void setID(int id) { this->m_id = id; }
	
	static void setID2(Simple* const hidden, int id) 
	{ 
		hidden->m_id = id; 
	}
};

struct Complex
{
	double Real;
	double Img;
};

struct Point
{
	double X;
	double Y;
};

void incapsulation()
{
	cout << endl << "'encapsulation' : " << endl;

	Simple s1(1);
	cout << "[After construction Simple s1(1)] s1.getID() = " << s1.getID() << endl;

	s1.setID(2);
	cout << "[After s1.setID(2)] s1.getID() = " << s1.getID() << endl;

	Simple::setID2(&s1, 3);
	cout << "[After static Simple::setID2(s1, 3)] s1.getID() = " << s1.getID() << endl;

	cin.get();
}

void poly()
{
	cout << endl << "'polymorphism' : " << endl;

	Point p;
	Complex c;

	c.Real = 123.0;
	c.Img = 0.456;

	Complex * ptr_c = &c;
	Point * ptr_p = (Point*)ptr_c;

	p = *ptr_p;

	cout << "Complex 'c' address : " << ptr_c << " ; Point 'p' address : " << ptr_p << endl;
	cout << "c.A  = " << c.Real << " ; c.B = " << c.Img << endl;
	cout << "p.A  = " << p.X << " ; p.B = " << p.Y << endl;

	cin.get();
}

int main()
{
	cout << "encapsulation and polymorphism" << endl;
	incapsulation();
	poly();

	return 0;
}

