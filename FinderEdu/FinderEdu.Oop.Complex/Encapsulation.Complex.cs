﻿#define step_4
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FinderEdu.Oop.Complex.Encapsulation
{

#if step_1
    public struct Complex
    {
        public double Real;
        public double Img;
    }
#endif

#if step_2
    public struct Complex
    {
        public double Real;
        public double Img;

        public override string ToString()
        {
            // $"" = equals to snprintf, printf, String.Format, etc...
            return $"{Real} + {Img}i";
        }
    }

    public static class ComplexMath
    {
        public static Complex Add(Complex left, Complex right)
        {
            Complex result = new Complex();
            result.Real = left.Real + right.Real;
            result.Img = left.Img + right.Img;
            return result;
        }

        public static Complex Sub(Complex left, Complex right)
        {
            var result = new Complex()
            {
                Real = left.Real - right.Real,
                Img = left.Img - right.Img
            };
            
            return result;
        }

        public static double Abs(Complex complex)
        {
            return Math.Sqrt(complex.Real * complex.Real + complex.Img * complex.Img);
        }
    }
#endif

#if step_3
    public struct Complex
    {
        public double Real;
        public double Img;

        /*
        public double GetAbs()
        {
            return ComplexMath.Abs(this);
        }
        */

        public override string ToString()
        {
            return $"{Real} + {Img}i";
        }
    }
#endif

#if step_4
    public struct Complex : IPoint
    {
        public double Real;
        public double Img;

        public double X { get { return Real; } }

        // X and Y notation is the same
        public double Y => Img;

        public double GetAbs()
        {
            return ComplexMath.Abs(this);
        }

        public override string ToString()
        {
            return $"{Real} + {Img}i";
        }
    }
#endif

#if step_3 || step_4

    public static class ComplexMath
    {
        public static Complex Add(Complex left, Complex right)
        {
            Complex result = new Complex();
            result.Real = left.Real + right.Real;
            result.Img = left.Img + right.Img;
            return result;
        }

        public static Complex Sub(Complex left, Complex right)
        {
            var result = new Complex()
            {
                Real = left.Real - right.Real,
                Img = left.Img - right.Img
            };
            
            return result;
        }

        public static double Abs(this Complex complex)
        {
            return Math.Sqrt(complex.Real * complex.Real + complex.Img * complex.Img);
        }
    }

#endif

    public static class Demo
    {
#if step_1
        public static void Show()
        {
            Complex c1 = new Complex
            {
                Real = 2,
                Img = 1
            };

            Console.WriteLine(
                "Complex c1.Real = {0}, c1.Img = {1}",
                c1.Real, 
                c1.Img);
        }
#endif

#if step_2
        public static void Show()
        {
            Complex c1 = new Complex() { Real = 2, Img = 1 };
            double c1Abs = ComplexMath.Abs(c1);

            Console.WriteLine($"Complex c1 = {c1}");
            Console.WriteLine($"Complex |c1| = {c1Abs}");
        }
#endif

#if step_3
        public static void Show()
        {
            Complex c1 = new Complex() { Real = 2, Img = 1 };
            
            // Without extension :
            double c1Abs2 = ComplexMath.Abs(c1);
            // With extension :
            double c1Abs = c1.Abs();

            Console.WriteLine($"Complex c1 = {c1}");
            Console.WriteLine($"Complex |c1| = {c1Abs}");
        }
#endif

#if step_4
        public static void Show()
        {
            Complex c1 = new Complex() { Real = 2, Img = 1 };
            Point p1 = new Point() { X = 12, Y = 34 };

            IPoint[] points = new IPoint[] { c1, p1 };

            Console.WriteLine("IPoint[] points contains :");


            foreach (IPoint point in points)
            {
                Console.WriteLine($"point({point.X}; {point.Y});");
            }

            //for (int i = 0; i < points.Length; i++)
            //{
            //    IPoint point = points[i];
            //    Console.WriteLine($"point({point.X}; {point.Y});");
            //}

        }
#endif
    }
}
