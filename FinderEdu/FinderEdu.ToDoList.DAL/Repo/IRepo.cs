﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinderEdu.ToDoList.DAL.Repo
{
    public interface IRepo<TKey, T> where TKey : struct
    {
        IQueryable<T> GetAll();
        Task<T> GetAsync(TKey key);
        Task InsertAsync(T entity);
        Task UpdateAsync(T entity);
        Task DeleteAsync(TKey key) ;
        Task DeleteAsync(T entity);
        Task SaveAsync();
        void StopTracking(T entity);
    }
}
