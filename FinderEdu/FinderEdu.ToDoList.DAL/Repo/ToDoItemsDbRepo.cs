﻿using FinderEdu.ToDoList.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using Microsoft.EntityFrameworkCore;

namespace FinderEdu.ToDoList.DAL.Repo
{
    public class ToDoItemsDbRepo : IRepo<Guid, ToDoItem>
    {
        private readonly DatabaseContext _context;
        private readonly DbSet<ToDoItem> _dbSet;

        public ToDoItemsDbRepo(DatabaseContext context)
        {
            _context = context;
            _dbSet = _context.ToDoItems;
        }

        public async Task DeleteAsync(Guid key)
        {
            var entity = await GetAsync(key);
            if (entity != null) await DeleteAsync(entity);
        }

        public Task DeleteAsync(ToDoItem entity)
        {
            _dbSet.Remove(entity);
            return Task.CompletedTask;
        }

        public IQueryable<ToDoItem> GetAll()
        {
            return _dbSet;
        }

        public async Task<ToDoItem> GetAsync(Guid key)
        {
            var result = await _dbSet.FindAsync(key);
            return result;
        }

        public Task InsertAsync(ToDoItem entity)
        {
            return _dbSet.AddAsync(entity);
        }

        public Task UpdateAsync(ToDoItem entity)
        {
            _dbSet.Update(entity);
            return Task.CompletedTask;
        }

        public Task SaveAsync()
        {
            return _context.SaveChangesAsync();
        }

        public void StopTracking(ToDoItem entity)
        {
            _context.Entry(entity).State = EntityState.Detached;
        }
    }
}
