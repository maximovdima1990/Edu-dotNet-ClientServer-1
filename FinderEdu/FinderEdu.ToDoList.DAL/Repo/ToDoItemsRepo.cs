﻿using FinderEdu.ToDoList.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace FinderEdu.ToDoList.DAL.Repo
{
    public class ToDoItemsRepo : IRepo<Guid, ToDoItem>
    {
        private static readonly ConcurrentDictionary<Guid, ToDoItem> _items =
            new ConcurrentDictionary<Guid, ToDoItem>();

        public ToDoItemsRepo()
        {
            Bootstrap();
        }

        private static void Bootstrap()
        {
            var now = DateTime.UtcNow;
            var id1 = new Guid("{00000000-0000-0000-0000-000000000001}");
            var id2 = new Guid("{00000000-0000-0000-0000-000000000002}");
            var ownerId = new Guid("{00000000-0000-0000-0000-000000000001}");
            _items.TryAdd(id1, new ToDoItem()
            {
                Id = id1,
                OwnerId = ownerId,
                Title = "Test title 1",
                Description = "Test description 1",
                CreatedAt = now,
                ModifiedAt = now
            });
            _items.TryAdd(id2, new ToDoItem()
            {
                Id = id2,
                OwnerId = ownerId,
                Title = "Test title 2",
                Description = "Test description 2",
                CreatedAt = now,
                ModifiedAt = now
            });
        }

        public Task DeleteAsync(Guid key)
        {
            _items.TryRemove(key, out ToDoItem removed);
            return Task.CompletedTask;
        }

        public Task DeleteAsync(ToDoItem entity)
        {
            var key = entity.Id;
            return DeleteAsync(key);
        }

        public IQueryable<ToDoItem> GetAll()
        {
            return _items.Values.ToArray().AsQueryable();
        }

        public Task<ToDoItem> GetAsync(Guid key)
        {
            var result = _items.TryGetValue(key, out ToDoItem item) ? item : default(ToDoItem) ;
            return Task.FromResult(result);
        }

        public Task InsertAsync(ToDoItem entity)
        {
            if (!_items.TryAdd(entity.Id, entity))
            {
                throw new InvalidOperationException("Can't insert entity to DB");
            }
            else return Task.CompletedTask;
        }

        public Task SaveAsync()
        {
            return Task.CompletedTask;
        }

        public void StopTracking(ToDoItem entity)
        {
            return; 
        }

        public Task UpdateAsync(ToDoItem entity)
        {
            if (!_items.ContainsKey(entity.Id))
            {
                throw new InvalidOperationException("Can't update entity in DB");
            }
            else 
            {
                _items[entity.Id] = entity;
            }
            return Task.CompletedTask;
        }
    }
}
