﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace FinderEdu.ToDoList.WpfClient
{
    public class ToDoItemsProxy
    {
        private HttpClient GetClient()
        {
            var client = new HttpClient()
            {
                BaseAddress = new Uri("http://localhost:55365/api/"),

            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }

        public async Task<List<ToDoItem>> LoadAllItemsAsync(Guid userId)
        {
            using (var client = GetClient())
            {
                var json = await client.GetStringAsync($"todoitems/{userId}");
                return JsonConvert.DeserializeObject<List<ToDoItem>>(json);
            }
        }

        public async Task AddOrUpdateItemAsync(Guid userId, ToDoItem item)
        {
            using (var client = GetClient())
            {
                var result = await client.PutAsync($"todoitems/{userId}", JsonContent.FromObject(item));
                result.EnsureSuccessStatusCode();
            }
        }

        public async Task DeleteItemAsync(Guid userId, Guid itemId)
        {
            using (var client = GetClient())
            {
                var result = await client.DeleteAsync($"todoitems/{userId}/{itemId}");
                result.EnsureSuccessStatusCode();
            }
        }
    }
}
