﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace FinderEdu.ToDoList.WpfClient
{
    public class MainViewModel : ObservableObject
    {
        private string _userIdText = "00000000-0000-0000-0000-000000000001";
        private bool _isBusy;
        private ObservableCollection<ToDoItem> _items;
        private ToDoItem _selectedItem;
        private ToDoItem _editingItem;

        public Command LoadCommand { get; protected set; }
        public Command AddCommand { get; protected set; }
        public Command EditCommand { get; protected set; }
        public Command DeleteCommand { get; protected set; }

        public ToDoItem EditingItem
        {
            get => _editingItem;
            protected set => SetProperty(ref _editingItem, value);
        }

        public ToDoItem SelectedItem
        {
            get => _selectedItem;
            set => SetProperty(ref _selectedItem, value);
        }

        public ObservableCollection<ToDoItem> Items
        {
            get => _items;
            protected set => SetProperty(ref _items, value);
        }

        public bool IsBusy
        {
            get => _isBusy;
            protected set => SetProperty(ref _isBusy, value, nameof(IsBusy), 
                ()=>OnPropertyChanged(nameof(IsBusyVisibility)));
        }

        public Visibility IsBusyVisibility { get => IsBusy ? Visibility.Visible : Visibility.Collapsed; }

        public string UserIdText
        {
            get => _userIdText;
            set => SetProperty(ref _userIdText, value);
        }

        public Guid? UserId { get => Guid.TryParse(UserIdText, out Guid id) ? id : default(Guid?); }

        public bool CanLoad { get => !IsBusy && UserId.HasValue; }
        public bool CanAdd { get => !IsBusy && UserId.HasValue; }
        public bool CanDelete { get => !IsBusy && SelectedItem != null && UserId.HasValue; }
        public bool CanEdit { get => !IsBusy && SelectedItem != null && UserId.HasValue; }

        public MainViewModel()
        {
            LoadCommand = Command.FromAsyncHandler(LoadAsync, () => CanLoad);
            AddCommand = Command.FromAsyncHandler(AddAsync, () => CanAdd);
            EditCommand = Command.FromAsyncHandler(EditAsync, () => CanEdit);
            DeleteCommand = Command.FromAsyncHandler(DeleteAsync, () => CanDelete);
        }

        public async Task LoadAsync()
        {
            if (!CanLoad) return;
            IsBusy = true;

            var server = new ToDoItemsProxy();
            var newItems = await server.LoadAllItemsAsync(UserId.Value);
            Items = new ObservableCollection<ToDoItem>(newItems.OrderByDescending(item=>item.ModifiedAt));
            SelectedItem = null;

            IsBusy = false;
        }

        protected async Task AddAsync()
        {
            if (!CanAdd) return;
            var dialog = new EditItemWindow();

            EditingItem = new ToDoItem();

            if (dialog.ShowDialog().GetValueOrDefault() && EditingItem!=null)
            {
                IsBusy = true;

                var server = new ToDoItemsProxy();
                await server.AddOrUpdateItemAsync(UserId.Value, EditingItem);

                if (Items == null) Items = new ObservableCollection<ToDoItem>();
                Items.Add(EditingItem);
                SelectedItem = EditingItem;
                EditingItem = null;

                IsBusy = false;
            }
        }

        protected async Task EditAsync()
        {
            var selected = SelectedItem;
            if (!CanEdit) return;

            var dialog = new EditItemWindow();
            EditingItem = new ToDoItem()
            {
                Id = selected.Id,
                Title = selected.Title,
                Description = selected.Description
            };

            if (dialog.ShowDialog().GetValueOrDefault() && EditingItem != null)
            {
                if (EditingItem != null)
                {
                    IsBusy = true;

                    var server = new ToDoItemsProxy();
                    await server.AddOrUpdateItemAsync(UserId.Value, EditingItem);

                    selected.Id = EditingItem.Id;
                    selected.Title = EditingItem.Title;
                    selected.Description = EditingItem.Description;

                    EditingItem = null;
                    IsBusy = false;
                }
            }
        }
        
        protected async Task DeleteAsync()
        {
            if (!CanDelete) return;
            var item = SelectedItem;

            IsBusy = true;

            var server = new ToDoItemsProxy();
            await server.DeleteItemAsync(UserId.Value, item.Id);

            Items.Remove(item);
            SelectedItem = null;

            IsBusy = false;
        }

        protected override void OnPropertyChangedInternalHandler(string propertyName)
        {
            base.OnPropertyChangedInternalHandler(propertyName);
            LoadCommand.RaiseCanExecuteChanged();
            AddCommand.RaiseCanExecuteChanged();
            EditCommand.RaiseCanExecuteChanged();
            DeleteCommand.RaiseCanExecuteChanged();
        }
    }
}
