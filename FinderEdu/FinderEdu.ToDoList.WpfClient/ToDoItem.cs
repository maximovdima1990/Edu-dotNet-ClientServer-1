﻿using System;

namespace FinderEdu.ToDoList.WpfClient
{
    public class ToDoItem : ObservableObject
    {
        private string _title;
        private string _desc;

        public Guid Id { get; set; }
        public string Title
        {
            get => _title;
            set
            {
                if (SetProperty(ref _title, value))
                    OnPropertyChanged(nameof(LongTitle));
            }
        }

        public string Description
        {
            get => _desc;
            set
            {
                if (SetProperty(ref _desc, value))
                    OnPropertyChanged(nameof(LongTitle));
            }
        }

        public string LongTitle => ToString();

        public DateTime ModifiedAt { get; set; }

        public ToDoItem()
        {
            Id = Guid.NewGuid();
        }

        public override string ToString()
        {
            return $"{Title} [{Description}]";
        }
    }
}
